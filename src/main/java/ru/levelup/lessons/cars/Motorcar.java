package ru.levelup.lessons.cars;

import annotation.RandomInt;

public class Motorcar {
    String brand;
    @RandomInt
    String type;
    int speed;

    public Motorcar() {
        this.brand = "Toyota Corolla";
        this.type = "Sedan";
        this.speed = 210;
    }

    public String toString() {
        String text = "Марка автомобиля: " + this.brand + " Тип кузова: " + this.type + " Максимальная скорость: " + this.speed + " km/h";
        return text;
    }

}
