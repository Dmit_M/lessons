package ru.levelup.lessons.cars;

import annotation.RandomInt;

public class Trucks {
    String brand;
    @RandomInt
    int speed;
    int lifting;

    public Trucks () {
        this.brand = "Kamaz";
        this.speed = 90;
        this.lifting = 10;
    }

    public String toString() {
        String text = "Марка автомобиля: " + this.brand +  " Максимальная скорость: " + this.speed + " km/h " + "Грузоподъемность: " + this.lifting + " t";
        return text;
    }

}

