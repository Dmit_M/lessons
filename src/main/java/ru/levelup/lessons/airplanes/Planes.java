package ru.levelup.lessons.airplanes;

import annotation.RandomInt;

public class Planes {
    String brand;
    @RandomInt
    int speed;
    @RandomInt
    int capacity;

    public Planes () {
        this.brand = "Ту-154";
        this.speed = 850;
        this.capacity = 180;
    }

    public String toString() {
        String text = "Самолет: " + this.brand + " Максимальная скорость: " + this.speed + " km/h " + " Количество пассажиров: " + this.capacity;
        return text;
    }

}

