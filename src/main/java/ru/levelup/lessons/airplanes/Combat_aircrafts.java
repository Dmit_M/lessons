package ru.levelup.lessons.airplanes;

import annotation.RandomInt;

public class Combat_aircrafts {
    @RandomInt
    int speed;
    @RandomInt
    int height;
    String brand;

    public Combat_aircrafts () {

        this.speed = 420;
        this.height = 5500;
        this.brand = "Ил-2";
    }

    public String toString() {
        String text = "Самолет : " + this.brand +  " Максимальная скорость: " + this.speed +" km/h " + " Потолок: " + this.height + " meters ";
        return text;
    }

}

