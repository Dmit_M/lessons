import annotation.RandomInt;
import annotation.RandomIntAnnotationProcessor;

import java.io.File;
import java.lang.reflect.InvocationTargetException;

public class Processes {

    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        File dir = new File(System.getProperty("user.dir")+"\\src\\main\\java\\ru\\levelup\\lessons");
        processFilesFromFolder(dir);
    }



    public static void processFilesFromFolder(File folder) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        File[] folderEntries = folder.listFiles();
        for (File entry : folderEntries)
        {
            if (entry.isDirectory())
            {
                processFilesFromFolder(entry);
                continue;
            }

          int index = entry.getAbsolutePath().indexOf("ru\\levelup\\lessons") + 19;
          String name = entry.getAbsolutePath().substring(index, entry.getAbsolutePath().length()-5);
          name = track("ru.levelup.lessons") + name.replace("\\", ".");

          Class<?> c = Class.forName(name);
          Object object = c.newInstance();
          RandomIntAnnotationProcessor.process(object);
          System.out.println(object.toString()); //? Почему метод toString() отработал при вызове через объект object,  без конструкции:

                                                     //  Method modelMethod = c.getDeclaredMethod("toString");
                                                     //  modelMethod.setAccessible(true);
                                                     //  System.out.println(modelMethod.invoke(object));
                                                     //                  ???

        }
    }
    public static String track(String folder) {
        String patch = folder+ ".";
        return patch;
    }



}
