package annotation;


import java.lang.reflect.Field;

public class RandomIntAnnotationProcessor {

    public static Object process(Object object) throws IllegalAccessException {
        Class<?> objClass = object.getClass();
        Field[] fields = objClass.getDeclaredFields();
        for (Field field : fields) {
            RandomInt annotation = field.getAnnotation(RandomInt.class);
            if (annotation != null) {
                if (field.getAnnotatedType().getType().getTypeName().equals("int") || (field.getAnnotatedType().getType().getTypeName().equals("java.lang.Integer"))) {
                    int min = annotation.min;
                    int max = annotation.max;
                    int random_number = min + (int) (Math.random() * max);

                    field.setAccessible(true);
                    field.set(object, random_number);
                } else {

                      throw new NullPointerException("Ошибка");
                }
            }
            }
        return object;
        }

    }
